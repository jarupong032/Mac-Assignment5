var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var app = express();

var urlencodeParser = bodyParser.urlencoded({extended:false});
app.get('/',function(req,res){
    res.send('Hello Node.js');
})

var con = mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"",
    database:"ass"
});


app.get('/api/articles',function(req,res){
    var sql = "SELECT * FROM ex_articles";
    con.query(sql,function(err,result){
        var tmp = JSON.stringify(result);
        res.send(tmp);
    })
})

app.get('/api/articles/:id',function(req,res){
    var sql = "SELECT * FROM ex_articles WHERE art_id = '"+req.params.id+"'";
    con.query(sql,function(err,result){
        var tmp = JSON.stringify(result);
        res.send(tmp);
    })
})

app.put('/api/articles/:id',urlencodeParser,function(req,res){
    var sql ="UPDATE ex_articles SET art_title = '"+req.body.art_title
    +"',art_description = '"+req.body.art_description
    +"'WHERE art_id = '"+req.params.id+"'";
    res.send(sql);
    con.query(sql,function(err,result){
        if(err){console.log("CANNOT UPDATE");}
        else{console.log("UPDATE go");}
    });
});

app.post('/api/articles',urlencodeParser,function(req,res){
    var sql = "INSERT INTO ex_articles (art_title,art_description) VALUE('"
    +req.body.art_title+"','"+req.body.art_description+"')";
    res.send(sql);
    con.query(sql,function(err){
         if(err){console.log("CANNOT INSERT");}
         else{console.log("1 row insert");}
    });
});

app.delete('/api/articles/:id',urlencodeParser,function(req,res){
    var sql ="DELETE FROM ex_articles WHERE art_id = '"+req.params.id+"'";
    con.query(sql,function(err,result){
        if(err){console.log("CANNOT DELETE");}
        else{console.log("DELETE go");}
    });
});


app.listen(3000,function(){
    console.log('port 3000');
});